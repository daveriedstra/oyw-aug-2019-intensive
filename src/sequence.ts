import TextScene from './text-scene';
import ParticleScene from './particle-scene';

import { VideoPool, Video, Pool, AudioPool, MediaPool, Asset } from 'pool-time';
import { pipe, merge } from 'rxjs';
import { take, last } from 'rxjs/operators';

const $ = document.querySelectorAll.bind(document);
const SRCS_ROOT = 'assets/video';
const stopVid = (v, p) => {
  v.el.classList.add('out');
  v.once('transitionend', () => p.destroyVideo(v))
}

/**
 * SEQUENCE
 *
 * 1. A discrete video
 * 2. A transitional video
 * 3. An animation
 */

function playDiscreteScene(container: HTMLElement): VideoPool {
  const SRC = 'FishWarfC0006-RegTime.mp4'
  const pool = new VideoPool({
    getDropTime: () => 0,
    duration: 40 * 1000,
    dropCount: 1,
    loop: false,
    assets: [{
      srcs: [`${SRCS_ROOT}/${SRC}`],
      formats: ['video/mp4']
    }]
  })

  let vid: Video
  pool.start$.subscribe(() => {
    pool.queue.forEach(v => {
      v.el.classList.add('out');
      container.append(v.el);
    })
  })

  pool.preloadAll()
  pool.play$.subscribe(v => {
    vid = v
    v.once('play', () => v.el.classList.remove('out'))
  });

  pool.stop$.subscribe(() => stopVid(vid, pool))

  pool.start()
  return pool
}

function playTransitionalScene(container: HTMLElement): VideoPool {
  const SRC = 'ThetisC0025-5xslow.mp4'
  const pool = new VideoPool({
    getDropTime: () => 0,
    duration: 50 * 1000,
    dropCount: 1,
    loop: false,
    assets: [{
      srcs: [`${SRCS_ROOT}/${SRC}`],
      formats: ['video/mp4']
    }]
  })

  let vid: Video
  pool.start$.subscribe(() => {
    pool.queue.forEach(v => {
      v.el.classList.add('out');
      container.append(v.el);
    })
  })

  pool.preloadAll()
  pool.play$.subscribe(v => {
    vid = v
    v.once('play', () => v.el.classList.remove('out'))
  });

  pool.stop$.subscribe(() => stopVid(vid, pool))

  pool.start()
  return pool
}

function playAnimationScene(container: HTMLElement) {
  const particleScene = new ParticleScene({
    particleCount: 2,
    ptArgs: {
      duration: 2 * 60 * 1000,
      dropCount: 25,
      getDropTime: dur => Math.sin(Math.random() * Math.PI) * dur,
      loop: true
    }
  })

  container.style.background = 'linear-gradient(160deg, #303133, #505153)'
  container.append(particleScene.el)
  particleScene.start()

  return particleScene
}

function makeMetaPool(): Pool {
  const pool = new Pool({
    duration: 2 * 60 * 1000,
    dropCount: 3,
    getDropTime: (dur, i) => i * 30 * 1000,
    loop: false
  })

  let last: Pool | ParticleScene

  pool.drop$.subscribe(i => {
    console.log(i)

    switch (i) {
      case 0:
        playDiscreteScene($('#video-container')[0])
        break;
      case 1:
        playTransitionalScene($('#video-container')[0])
        break;
      case 2:
        playAnimationScene($('#video-container')[0])
        break;
    }
  })

  return pool
}

let paused = false
function init() {
  // get elements
  const EL_PLAY_LINK = $('#start')[0];
  const EL_PAUSE_LINK = $('#pause')[0];
  const metaPool = makeMetaPool()

  EL_PLAY_LINK.classList.remove('disabled')

  // play
  EL_PLAY_LINK.addEventListener('click', e => {
    metaPool.start()

    e.target.classList.add('hidden');
    document.body.classList.add('playing')
    EL_PAUSE_LINK.classList.remove('hidden');
  });

  // pause toggle
  EL_PAUSE_LINK.addEventListener('click', e => {
    paused = !paused;
    paused ? metaPool.pause() : metaPool.resume();

    e.target.innerText = paused ? 'resume' : 'pause';
    document.body.classList[paused ? 'remove' : 'add']('playing')
  });

}

init();

import { VideoPool, Video, Pool, AudioPool, MediaPool, Asset } from 'pool-time';
import { pipe, merge } from 'rxjs';
import { take, last } from 'rxjs/operators';

const SRCS_ROOT = 'assets/video';
const BASE_SRCS = [
  'FishWarfC0006-5xslow.mp4', // 8:25
  'FishWarfC0017-5xslow.mp4', // 10:39
  'ThetisC0026-5xslow.mp4',   // 13:31
]
const $ = document.querySelectorAll.bind(document);

function makeBasePool(): VideoPool {
  const EL_VID_CONTAINER = $('#video-container')[0];
  const basePool = new VideoPool({
    ordered: true,
    getDropTime: (dur, i) => i * dur / 3,
    duration: 6 * 60 * 1000,
    // duration: 6 * 1000,
    dropCount: 3,
    loop: false,
    assets: BASE_SRCS.map(s => ({
      srcs: [`${SRCS_ROOT}/${s}`],
      formats: ['video/mp4'],
    }) as Asset)
  });

  const stopVid = vid => {
    vid.el.classList.add('out');
    vid.once('transitionend', () => basePool.destroyVideo(vid))
  }

  let started: Video[] = [];
  let lastPlaying: Video;

  basePool.start$.subscribe(() => {
    basePool.queue.forEach(v => {
      v.el.classList.add('out');
      // EL_VID_CONTAINER.prepend(v.el);
      EL_VID_CONTAINER.append(v.el);
      v.el.load()
    })
  });

  basePool.play$.subscribe(nextVideo => {
    nextVideo.el.classList.remove('out')

    // stop last video
    if (lastPlaying)
      stopVid(lastPlaying)

    // stash this video
    lastPlaying = nextVideo;
  });

  basePool.stop$.subscribe(() => {
    if (lastPlaying)
      stopVid(lastPlaying)
    lastPlaying = undefined;
  })
  return basePool;
}

let paused = false;
function init() {
  // get elements
  const EL_PLAY_LINK = $('#start')[0];
  const EL_PAUSE_LINK = $('#pause')[0];

  // make pools
  const pools = [
    makeBasePool(),
    // makeOverlayPool()
  ]

  EL_PLAY_LINK.classList.add('disabled')
  const load$ = pools.map(p => p.preloadAll())
  merge(load$)
    .pipe(last())
    .subscribe(() => EL_PLAY_LINK.classList.remove('disabled') )

  // play
  EL_PLAY_LINK.addEventListener('click', e => {
    pools.forEach(p => p.start())

    e.target.classList.add('hidden');
    document.body.classList.add('playing')
    EL_PAUSE_LINK.classList.remove('hidden');
  });

  // pause toggle
  EL_PAUSE_LINK.addEventListener('click', e => {
    paused = !paused;
    pools.forEach(p => paused ? p.pause() : p.resume());

    e.target.innerText = paused ? 'resume' : 'pause';
    document.body.classList[paused ? 'remove' : 'add']('playing')
  });

}

init();

import { Pool } from "pool-time";

function randBetween(lo, hi, int = false) {
    const out = Math.random() * (hi - lo) + lo;
    return int ? Math.round(out) : out;
}

export default class ParticleScene {
  pool: Pool
  el: HTMLDivElement = document.createElement('div')

  private getVisibleOpacity = () => randBetween(0.2, 0.7)

  constructor ({ particleCount, ptArgs }) {
    this.pool = new Pool(ptArgs)
    this.prepareContainer()

    this.pool.drop$.subscribe(() => {
      const top = randBetween(10, 90)
      const left = randBetween(10, 90)
      const count = randBetween(1, particleCount, true)
      const particles = [...Array(count)].map(() => this.makeParticle({top, left}))

      particles.map(p => this.showParticle(p))
    })
  }

  start() { this.pool.start() }
  pause() { this.pool.pause() }
  resume() { this.pool.resume() }
  stop() { this.pool.stop() }


  /**
   * Makes a particle
   */
  private makeParticle({top, left}) {
    const div = document.createElement('div');
    div.classList.add('particle');

    div.style.top = `${top}vh`
    div.style.left = `${left}vw`
    div.style.opacity = '0'

    const size = randBetween(5, 33.333)
    const color = '#fff'
    div.style.boxShadow = `0 0 ${size}vw ${size / 2}vw ${color}`
    div.style.background = color

    return div
  }

  /**
   * Adds a particle to the DOM, starts transitions,
   * queues up its removal
   */
  private showParticle(p: HTMLElement) {
    const dur = randBetween(6 * 1000, 7 * 1000)
    const dist = 20
    const tx = Math.round(Math.random() * 2 * dist) - dist
    const ty = Math.round(Math.random() * 2 * dist) - dist

    p.style.transition = `transform ${dur}ms cubic-bezier(.23,1,.32,1), opacity ${dur/3}ms ease-in-out`;

    this.el.appendChild(p)
    setTimeout(() => {
      p.style.opacity = `${randBetween(0.2, 0.8)}`
      p.style.transform = `translate(${tx}vw, ${ty}vw)`
    }, 20)

    setTimeout(() => this.removeParticle(p), dur * 2 / 3)
  }

  /**
   * Removes particle from DOM with nice fade out action
   */
  private removeParticle(p: HTMLElement) {
    const addRm = () => {
      const rm = e => {
        if (e.propertyName === 'opacity') {
          p.removeEventListener('transitionend', rm)
          p.remove()
        }
      }
      p.addEventListener('transitionend', rm)
      p.removeEventListener('transitionstart', addRm)
    }
    p.addEventListener('transitionstart', addRm)
    p.style.opacity = '0'
  }

  private prepareContainer() {
    this.el.classList.add('particle-scene')
  }
}


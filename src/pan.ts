import { HowlReference, VideoPool, Video, Pool, AudioPool, MediaPool, Asset } from 'pool-time';
import { pipe, merge } from 'rxjs';
import { take, last } from 'rxjs/operators';
import { Howl } from 'howler';

import { Panner, IPanToArgs } from './panner';

const $ = document.querySelectorAll.bind(document);
const SEC = 1000
const MIN = 60 * SEC
const SRCS = [
  'https://daveriedstra.com/dev/assets/audio/OnYourWinds-IndividualBreathFiles/Breaths-Complete-Edited-NotStretched/webm/laura-wind-2.webm',
  'https://daveriedstra.com/dev/assets/audio/OnYourWinds-IndividualBreathFiles/Breaths-Complete-Edited-NotStretched/webm/WindE-George-noiseReduced.webm',
  'https://daveriedstra.com/dev/assets/audio/OnYourWinds-IndividualBreathFiles/Stretched-Breath_Segments/webm/aaKnutStretchx12+EQ.webm',
  'https://daveriedstra.com/dev/assets/audio/tettix-4.mp3'
]

let howlRef: HowlReference;
let x = 0,
  y = 0,
  z = 1,
  duration = 5000;
let ctlX = 0,
  ctlY = 0,
  ctlZ = 0

function makeAudioPool(srcUrl: string) {
  const p = new AudioPool({
    assets: [{
      srcs: [ srcUrl ]
    }],
    getDropTime: (dur, i) => i * 10 * SEC,
    dropCount: 1,
    // duration: 3 * MIN + 38 * SEC,
    duration: 1 * MIN,
    ordered: true,
    loop: true
  })

  const load$ = p.preloadAll()

  p.play$.subscribe(hr => {
    console.log('playing', hr)
    hr.howl.pos(x, y, z, hr.id)

    if (howlRef)
      howlRef.howl.stop(howlRef.id)

    howlRef = hr
  })

  const chunks = srcUrl.split('/')
  $("#current-src")[0].innerText = chunks[chunks.length - 1]

  return {
    pool: p,
    load$: load$
  }
}

/**
 * get bezier point on x-z plane by 'curve intensity' value
 */
function curveDegreeToXZCtlPoint({ degree, startX, startZ, endX, endZ }) {
  const halfX = (endX - startX) / 2 + startX
  const halfZ = (endZ - startZ) / 2 + startZ

  // highschool me would be ashamed
  // y = mx + b
  // get slope of original line
  const slope = endX == startX ? Infinity : (endZ - startZ) / (endX - startX)
  // perpendicular slope
  const m = -1 / slope

  // thanks https://www.geeksforgeeks.org/find-points-at-a-given-distance-on-a-line-of-given-slope/
  const dist = 2 * degree
  let dx = 0,
    dz = 0

  if (m === 0) {
    dx = dist
  } else if (!isFinite(m)) {
    dz = dist
  } else {
    dx = (dist / Math.sqrt(1 + m ** 2))
    dz = m * dx
  }

  const handleX = halfX + dx
  const handleZ = halfZ + dz

  return { x: handleX, z: handleZ }
}

let paused = false
function init() {
  // get elements
  const EL_PLAY_LINK = $('#start')[0];
  const EL_PAUSE_LINK = $('#pause')[0];
  let audioPool = makeAudioPool(SRCS[0]).pool
  let audioIsStarted = false
  let STOP_SPINNING = false

  EL_PLAY_LINK.classList.remove('disabled')

  //
  // cfg controls
  //
  $('#position__x')[0].value = x
  $('#position__y')[0].value = y
  $('#position__z')[0].value = z
  $('#position__duration')[0].value = duration

  $('#bezier__x')[0].value = ctlX
  $('#bezier__y')[0].value = ctlY
  $('#bezier__z')[0].value = ctlZ

  // init srcs selector
  SRCS.forEach(src => {
    const el = document.createElement('li')
    const chunks = src.split('/')
    el.innerText = chunks[chunks.length - 1]
    el.addEventListener('click', () => {
      console.log(`loading ${src}`)
      audioPool.stop()
      let { pool, load$ } = makeAudioPool(src)
      audioPool = pool
      if (audioIsStarted)
        load$.subscribe(() => startAudio())
    })
    $("#srcs-selector ul")[0].appendChild(el)
  })

  /**
   * Starts the audio playback and updates playback control appearance
   */
  function startAudio() {
    audioPool.start()
    EL_PLAY_LINK.classList.add('hidden')
    document.body.classList.add('playing')
    EL_PAUSE_LINK.classList.remove('hidden')
    audioIsStarted = true
  }

  /**
   * Performs the pan specified by the target and bezier control points.
   * Simply gets those values and plugs them into Panner.moveTo(),
   * then immediately updates the current position to the target values.
   */
  function doPan() {
    const _x = +$('#position__x')[0].value
    const _y = +$('#position__y')[0].value
    const _z = +$('#position__z')[0].value

    ctlX = +$('#bezier__x')[0].value
    ctlY = +$('#bezier__y')[0].value
    ctlZ = +$('#bezier__z')[0].value

    STOP_SPINNING = true

    Panner.moveTo({
      howlRef: howlRef,

      startX: x,
      endX: _x,

      startY: y,
      endY: _y,

      startZ: z,
      endZ: _z,

      ctlX: ctlX === 0 ? undefined : ctlX,
      ctlY: ctlY === 0 ? undefined : ctlY,
      ctlZ: ctlZ === 0 ? undefined : ctlZ,

      duration: +$('#position__duration')[0].value
    })
      .subscribe(() => console.log('completed'))

    x = _x
    y = _y
    z = _z
  }

  /**
   * Performs a pre-programmed spin around the user and moving from above to
   * below.
   * Spin is composed of curve components which recurse - the next curve is
   * started when the current one ends.
   */
  function doSpin() {
    const TOP = 20, BOTTOM = -20
    const start = { x: -5, y: TOP, z: 0 }
    const end = { x: 5, y: TOP, z: 0 }

    let degree = -10
    let downward = true

    howlRef.howl.pos(start.x, start.y, start.z, howlRef.id)

    const doMvt = () => {
      if (STOP_SPINNING)
        return

      const ctl = curveDegreeToXZCtlPoint({
        degree: degree,
        startX: start.x, startZ: start.z,
        endX: end.x, endZ: end.z
      })

      Panner.moveTo({
        howlRef: howlRef,
        startX: start.x, startY: start.y, startZ: start.z,
        endX: end.x, endY: end.y, endZ: end.z,
        ctlX: ctl.x, ctlZ: ctl.z,
        duration: 3 * SEC
      })
        .subscribe(() => {
          // swap start and end X
          const _x = start.x
          start.x = end.x
          end.x = _x

          // downward or upward?
          downward = (end.y < start.y && end.y > BOTTOM) || end.y >= TOP

          // decrease Y
          start.y = end.y
          const factor = downward ? -1 : 1
          end.y += factor

          // invert curve degree
          degree *= -1

          // call self
          doMvt()
        })
    }

    STOP_SPINNING = false
    doMvt()
  }

  /**
   * Update the displayed current position text
   */
  function doUpdateCurrentPosition() {
    let pos = {
      x: x,
      y: y,
      z: z
    }
    if (Panner.MOVING_SOUNDS.length > 0) {
      pos.x = Panner.MOVING_SOUNDS[0].x.current
      pos.y = Panner.MOVING_SOUNDS[0].y.current
      pos.z = Panner.MOVING_SOUNDS[0].z.current
    }

    $("#current-position")[0].innerText = `Current position: (${Number(pos.x).toFixed(2)}, ${Number(pos.y).toFixed(2)}, ${Number(pos.z).toFixed(2)})`
  }

  // event handler for play control
  EL_PLAY_LINK.addEventListener('click', e => {
    startAudio()
    e.preventDefault()
  });

  // event handler for pause toggle
  EL_PAUSE_LINK.addEventListener('click', e => {
    paused = !paused;
    paused ? audioPool.pause() : audioPool.resume();

    e.target.innerText = paused ? 'resume' : 'pause';
    document.body.classList[paused ? 'remove' : 'add']('playing')
    e.preventDefault()
  });

  // event handler for Go button
  $('#position__go')[0].addEventListener('click', e => {
    if (audioIsStarted) {
      doPan()
    } else {
      audioPool.play$.pipe(take(1)).subscribe(doPan)
      startAudio()
    }
  })

  // event handler for Do a Spin button
  $('#do_spin')[0].addEventListener('click', e => {
    if (audioIsStarted) {
      doSpin()
    } else {
      audioPool.play$.pipe(take(1)).subscribe(doSpin)
      startAudio()
    }
  });

  // event handler for curve intensity value change
  $('#bezier__degree')[0].addEventListener('change', e => {
    const curveDegree = +e.target.value

    const startX = x
    const startZ = z

    const endX = +$('#position__x')[0].value
    const endZ = +$('#position__z')[0].value

    const handle = curveDegreeToXZCtlPoint({
      degree: curveDegree,
      startX: startX,
      startZ: startZ,
      endX: endX,
      endZ: endZ
    })

    $('#bezier__x')[0].value = ctlX = handle.x
    $('#bezier__z')[0].value = ctlZ = handle.z
  })

  // timer to update current position text
  window.setInterval(doUpdateCurrentPosition, 500)
}

init();

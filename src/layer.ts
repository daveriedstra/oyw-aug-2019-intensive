import { VideoPool, Video, Pool, AudioPool, MediaPool, Asset } from 'pool-time';
import { pipe, merge } from 'rxjs';
import { take, last } from 'rxjs/operators';

const SRCS_ROOT = 'assets/video';
const SRCS = {
  fishWarf: {
    slow: 'L-FishWharf/L-FishWarfC0006-1fifth.mp4',
    med:  'L-FishWharf/L-FishWarfC0006-3fifths.mp4',
    fast: 'L-FishWharf/L-FishWarfC0006-RegTime.mp4'
  },
  thetis: {
    slow: 'L-Thetis/L-ThetisC0026-1fifth-ColCor6Aug.mp4',
    med:  'L-Thetis/L-ThetisC0026-3fifths-Color6Aug.mp4',
    fast: 'L-Thetis/L-ThetisC0026-5fifths-Color6Aug-.mp4'
  },
}
const $ = document.querySelectorAll.bind(document);
let EL_VID_CONTAINER: HTMLDivElement;

let paused = false;
function init() {
  // get elements
  const EL_PLAY_LINK = $('#start')[0];
  const EL_PAUSE_LINK = $('#pause')[0];

  // get source urls
  let activeSrcs = SRCS.fishWarf;
  if (location.search.includes('thetis'))
    activeSrcs = SRCS.thetis;

  const fName = {
    slow: `${SRCS_ROOT}/${activeSrcs.slow}`,
    med: `${SRCS_ROOT}/${activeSrcs.med}`,
    fast: `${SRCS_ROOT}/${activeSrcs.fast}`,
  }

  // make pools
  const vids = [
    new Video({
        srcs: [fName.slow],
        formats: ['video/mp4']
    }),
    new Video({
        srcs: [fName.med],
        formats: ['video/mp4']
    }),
    new Video({
        srcs: [fName.fast],
        formats: ['video/mp4']
    })
  ]

  vids.forEach((v, i) => {
    v.el.load();
    $('#video-container')[0].appendChild(v.el);

    if (i)
      v.el.style.opacity = '0.5';

    // test loop
    v.el.addEventListener('timeupdate', e => {
      if (v.el.currentTime > (v.el.duration - 2)) {
        v.el.currentTime = 0.3;
        console.log('manually looped', v.src)
      }
    });
  })

  EL_PLAY_LINK.classList.remove('disabled')

  // play
  EL_PLAY_LINK.addEventListener('click', e => {
    vids.forEach(v => {
      v.play()
      v.el.classList.remove('out')
    })

    e.target.classList.add('hidden');
    document.body.classList.add('playing')
    EL_PAUSE_LINK.classList.remove('hidden');
  });

  // pause toggle
  EL_PAUSE_LINK.addEventListener('click', e => {
    paused = !paused;
    vids.forEach(v => paused ? v.pause() : v.play());

    e.target.innerText = paused ? 'resume' : 'pause';
    document.body.classList[paused ? 'remove' : 'add']('playing')
  });

}

init();

import { Pool } from "pool-time";

export default class TextScene {
  pool: Pool
  el: HTMLDivElement = document.createElement('div')

  private getTextVisibleOpacity = () => Math.random() * 0.4 + 0.4

  constructor ({ texts, ptArgs }) {
    this.pool = new Pool(ptArgs)

    this.prepareContainer()

    this.pool.drop$.subscribe(() => {
      const text = texts[Math.floor(Math.random() * texts.length)]
      const p = this.makeTextEl(text)
      this.showTextEl(p)

      setTimeout(() => this.hideTextEl(p), 6 * 1000)
    })
  }

  start() { this.pool.start() }
  pause() { this.pool.pause() }
  resume() { this.pool.resume() }

  private makeTextEl(text: string) {
    const p = document.createElement('p')
    p.innerText = text
    p.classList.add('text-scene__p')
    p.style.opacity = '0'

    return p
  }

  private showTextEl(p: HTMLParagraphElement) {
    const x = Math.round(Math.random() * 50)
    const y = Math.round(Math.random() * 75)
    p.style.top = `${x}vw`
    p.style.left = `${y}vh`

    const tx = Math.round(Math.random() * 50) - 25
    const ty = Math.round(Math.random() * 50) - 25

    this.el.appendChild(p)
    setTimeout(() => {
      p.style.opacity = `${this.getTextVisibleOpacity()}`
      p.style.transform = `translate(${tx}vw, ${ty}vh)`
    }, 20)
  }

  private hideTextEl(p: HTMLParagraphElement) {
    const addRm = () => {
      const rm = () => {
        p.removeEventListener('transitionend', rm)
        p.remove()
      }
      p.addEventListener('transitionend', rm)
      p.removeEventListener('transitionstart', addRm)
    }
    p.addEventListener('transitionstart', addRm)
    p.style.opacity = '0'
  }

  private prepareContainer() {
    this.el.classList.add('text-scene')
  }
}

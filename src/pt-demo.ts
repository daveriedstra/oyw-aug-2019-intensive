import { VideoPool, Video, Pool, AudioPool, MediaPool, Asset } from 'pool-time';
import { pipe, merge } from 'rxjs';
import { take, last } from 'rxjs/operators';

const SRCS_ROOT = 'assets/video';
const BASE_SRCS = [
  'FishWarfC0006-5xslow.mp4',
  'ThetisC0025-5xslow.mp4',
]
const OVERLAY_SRCS = [
  'FishWarfC0006-RegTime.mp4',
  'FishWarfC0017-RegTime.mp4',
  'ThetisC0012-RegTime.mp4',
  'ThetisC0018-RegTime.mp4',
  'ThetisC0025-RegTime.mp4',
]
const $ = document.querySelectorAll.bind(document);

function makeBasePool(): VideoPool {
  const EL_VID_CONTAINER = $('#video-container')[0];
  const basePool = new VideoPool({
    getDropTime: () => 0,
    duration: 30 * 1000,
    dropCount: 1,
    loop: true,
    assets: BASE_SRCS.map(s => ({
      srcs: [`${SRCS_ROOT}/${s}`],
      formats: ['video/mp4'],
      seek: d => d * Math.random() * (2/3)
    }) as Asset)
  });

  basePool.start$.subscribe(() => {
    basePool.queue.forEach(v => {
      v.el.classList.add('out');
      EL_VID_CONTAINER.appendChild(v.el);

      // manually loop when in last second of duration
      v.el.addEventListener('timeupdate', e => {
        if (v.el.currentTime > (v.el.duration - 1)) {
          v.el.currentTime = 0.3;
          console.log('loop\'d')
        }
      });
    })
  });

  basePool.play$.subscribe(nextVideo => {
    nextVideo.once('play', e => e.target.classList.remove('out'))
  });

  basePool.stop$.subscribe(() => {
    basePool.queue.forEach(vid => {
      vid.once('transitionend', () => basePool.destroyVideo(vid))
      vid.el.classList.add('out');
    });
  })
  return basePool;
}

function makeOverlayPool(): VideoPool {
  const EL_OVERLAY_CONTAINER = $('#overlay-container')[0];
  const overlayPool = new VideoPool({
    // getDropTime: () => 0,
    duration: 10 * 1000,
    dropCount: 1,
    loop: true,
    assets: OVERLAY_SRCS.map(s => ({
      srcs: [`${SRCS_ROOT}/${s}`],
      formats: ['video/mp4'],
      seek: d => d * Math.random() * (2/3)
    }) as Asset)
  });

  overlayPool.start$.subscribe(() => {
    overlayPool.queue.forEach(v => {
      v.el.classList.add('out');
      EL_OVERLAY_CONTAINER.appendChild(v.el);
    })
  });

  // play a short video overlay
  overlayPool.play$.subscribe(nextVideo => {
    nextVideo.once('play', e => e.target.classList.remove('out'))

    nextVideo.after(5, e => {
      nextVideo.once('transitionend', () => overlayPool.destroyVideo(nextVideo))
      nextVideo.el.classList.add('out');
    })
  });
  return overlayPool;
}

let paused = false;
function init() {
  // get elements
  const EL_PLAY_LINK = $('#start')[0];
  const EL_PAUSE_LINK = $('#pause')[0];

  // make pools
  const pools = [
    makeBasePool(),
    makeOverlayPool()
  ]

  EL_PLAY_LINK.classList.add('disabled')
  const load$ = pools.map(p => p.preloadAll())
  merge(load$)
    .pipe(last())
    .subscribe(() => EL_PLAY_LINK.classList.remove('disabled') )

  // play
  EL_PLAY_LINK.addEventListener('click', e => {
    pools.forEach(p => p.start())

    e.target.classList.add('hidden');
    document.body.classList.add('playing')
    EL_PAUSE_LINK.classList.remove('hidden');
  });

  // pause toggle
  EL_PAUSE_LINK.addEventListener('click', e => {
    paused = !paused;
    pools.forEach(p => paused ? p.pause() : p.resume());

    e.target.innerText = paused ? 'resume' : 'pause';
    document.body.classList[paused ? 'remove' : 'add']('playing')
  });

}

init();

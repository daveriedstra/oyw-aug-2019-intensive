import { VideoPool, Video, Pool, AudioPool, MediaPool, Asset } from 'pool-time';
import { pipe, merge } from 'rxjs';
import { take, last } from 'rxjs/operators';

const SRCS_ROOT = 'assets/video';
const SUFFIX = {
  slow: '5xslow.mp4',
  fast: 'RegTime.mp4'
}
const SRCS = [
  { slow: 'ThetisC0025-5xslow.mp4', fast: 'ThetisC0025-RegTime.mp4' },
  { slow: 'FishWarfC0006-5xslow.mp4', fast: 'FishWarfC0006-RegTime.mp4' },
]
const $ = document.querySelectorAll.bind(document);
let EL_VID_CONTAINER: HTMLDivElement;

let paused = false;
function init() {
  // get elements
  const EL_PLAY_LINK = $('#start')[0];
  const EL_PAUSE_LINK = $('#pause')[0];

  // get source urls
  let baseFName = 'FishWarfC0006';
  const query = location.search.split('?')
  if (query.length > 1)
    baseFName = query[1];

  const fName = {
    slow: `${SRCS_ROOT}/${baseFName}-${SUFFIX.slow}`,
    fast: `${SRCS_ROOT}/${baseFName}-${SUFFIX.fast}`,
  }

  // make pools
  const slowVid = new Video({
      srcs: [fName.slow],
      formats: ['video/mp4']
  });
  const fastVid = new Video({
      srcs: [fName.fast],
      formats: ['video/mp4']
  });

  slowVid.el.load();
  slowVid.el.classList.add('out');
  $('#video-container')[0].appendChild(slowVid.el);

  // test loop
  slowVid.el.addEventListener('timeupdate', e => {
    if (slowVid.el.currentTime > (slowVid.el.duration - 2)) {
      slowVid.el.currentTime = 0.3;
      console.log('manually looped')
    }
  });

  fastVid.el.load();
  fastVid.el.classList.add('out');
  $('#video-container')[0].appendChild(fastVid.el);

  EL_PLAY_LINK.classList.remove('disabled')

  // play
  EL_PLAY_LINK.addEventListener('click', e => {
    slowVid.play()
    slowVid.el.classList.remove('out')

    e.target.classList.add('hidden');
    document.body.classList.add('playing')
    EL_PAUSE_LINK.classList.remove('hidden');

    document.body.addEventListener('mousedown', () => {
      const seekRatio = slowVid.el.currentTime / slowVid.el.duration;
      fastVid.el.currentTime = seekRatio * fastVid.el.duration;
      fastVid.el.load();

      fastVid.playWhenReady();
      fastVid.el.style.opacity = '0.5';
    })

    document.body.addEventListener('mouseup', () => {
      fastVid.el.style.opacity = '';
      fastVid.once('transitionend', () => fastVid.pause())
    })
  });

  // pause toggle
  EL_PAUSE_LINK.addEventListener('click', e => {
    paused = !paused;
    paused ? slowVid.pause() : slowVid.play();

    e.target.innerText = paused ? 'resume' : 'pause';
    document.body.classList[paused ? 'remove' : 'add']('playing')
  });

}

init();

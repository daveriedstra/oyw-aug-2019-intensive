import { VideoPool, Video, Pool, AudioPool, MediaPool, Asset } from 'pool-time';
import { pipe, merge } from 'rxjs';
import { take, last } from 'rxjs/operators';

const SRC = 'https://190719test.nyc3.digitaloceanspaces.com/ThetisC0026-5xslow.mp4';
// const SRC = 'https://190719test.nyc3.digitaloceanspaces.com/oyw-test/2019/07/C0017-1080.mp4';
const $ = document.querySelectorAll.bind(document);

function makePool(): VideoPool {
  const EL_VID_CONTAINER = $('#video-container')[0];
  const pool = new VideoPool({
    ordered: true,
    getDropTime: () => 0,
    duration: 2 * 60 * 1000,
    // duration: 20 * 1000,
    dropCount: 1,
    loop: true,
    assets: [{
      srcs: [SRC],
      formats: ['video/mp4'],
      seek: dur => {
        let t = Math.random() * 0.8 * dur
        console.log('seek', t)
        return t
      }
    }]
  });

  const stopVid = vid => {
    vid.el.classList.add('out');
    vid.once('transitionend', () => pool.destroyVideo(vid))
  }

  let lastPlaying: Video;

  pool.start$.subscribe(() => {
    pool.queue.forEach(v => {
      v.el.style.transition = 'opacity 10s ease-in-out'
      v.el.classList.add('out')
      console.log('currentTime', v.el.currentTime)
      EL_VID_CONTAINER.prepend(v.el)
      v.el.load()
    })
  });

  pool.play$.subscribe(nextVideo => {
    nextVideo.el.classList.remove('out')

    // stop last video
    if (lastPlaying)
      stopVid(lastPlaying)

    // stash this video
    lastPlaying = nextVideo;
  });

  pool.stop$.subscribe(() => {
    if (lastPlaying)
      stopVid(lastPlaying)
    lastPlaying = undefined;
  })
  return pool;
}

let paused = false;
function init() {
  // get elements
  const EL_PLAY_LINK = $('#start')[0];
  const EL_PAUSE_LINK = $('#pause')[0];

  // make pools
  const pools = [
    makePool()
  ]

  EL_PLAY_LINK.classList.add('disabled')
  const load$ = pools.map(p => p.preloadAll())
  merge(load$)
    .pipe(last())
    .subscribe(() => EL_PLAY_LINK.classList.remove('disabled') )

  // play
  EL_PLAY_LINK.addEventListener('click', e => {
    pools.forEach(p => p.start())

    e.target.classList.add('hidden');
    document.body.classList.add('playing')
    EL_PAUSE_LINK.classList.remove('hidden');
  });

  // pause toggle
  EL_PAUSE_LINK.addEventListener('click', e => {
    paused = !paused;
    pools.forEach(p => paused ? p.pause() : p.resume());

    e.target.innerText = paused ? 'resume' : 'pause';
    document.body.classList[paused ? 'remove' : 'add']('playing')
  });
}

init();

const HtmlWebpackPlugin = require('html-webpack-plugin')
const path = require('path');

module.exports = {
  mode: 'development',
  entry: {
    main: './src/main.ts',
    'pt-demo': './src/pt-demo.ts',
    chain: './src/chain.ts',
    'chain-long': './src/chain-long.ts',
    layer: './src/layer.ts',
    pan: './src/pan.ts',
    sequence: './src/sequence.ts'
  },
  devtool: 'inline-source-map',
  module: {
    rules: [{
      test: /\.ts$/,
      use: 'ts-loader',
      exclude: /node_modules/
    }, {
      test: /\.mjs$/,
      type: 'javascript/auto'
    }]
  },
  resolve: {
    extensions: [ '.ts', '.js' ]
  },
  devServer: {
    host: '0.0.0.0',
    useLocalIp: true,
    writeToDisk: false
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './index.html',
      inject: true,
      chunks: ['main'],
      filename: 'index.html'
    }),
    new HtmlWebpackPlugin({
      template: './index.html',
      inject: true,
      chunks: ['chain'],
      filename: 'chain.html'
    }),
    new HtmlWebpackPlugin({
      template: './index.html',
      inject: true,
      chunks: ['chain-long'],
      filename: 'chain-long.html'
    }),
    new HtmlWebpackPlugin({
      template: './index.html',
      inject: true,
      chunks: ['layer'],
      filename: 'layer.html'
    }),
    new HtmlWebpackPlugin({
      template: './index.html',
      inject: true,
      chunks: ['sequence'],
      filename: 'sequence.html'
    }),
    new HtmlWebpackPlugin({
      template: './panning-demo.html',
      inject: true,
      chunks: ['pan'],
      filename: 'pan.html'
    }),
    new HtmlWebpackPlugin({
      template: './index.html',
      inject: true,
      chunks: ['pt-demo'],
      filename: 'pt-demo.html'
    })
  ]
};
